package com.obi.awesomekumparan.data.response.users

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
class AlbumResponse (
    @SerializedName("userId")
    var userId: String,
    @SerializedName("id")
    var id: String,
    @SerializedName("title")
    var title: String
)