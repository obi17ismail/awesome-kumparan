package com.obi.awesomekumparan.data.response.posts

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class DataPostResponse (
    @SerializedName("userId")
    var userId: String,
    @SerializedName("id")
    var id: String,
    @SerializedName("title")
    var title: String,
    @SerializedName("body")
    var body: String
)