package com.obi.awesomekumparan.data.uiresponse

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}