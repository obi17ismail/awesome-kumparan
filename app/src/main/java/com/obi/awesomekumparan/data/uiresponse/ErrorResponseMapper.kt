package com.obi.awesomekumparan.data.uiresponse

import com.google.gson.Gson
import com.obi.awesomekumparan.data.response.ErrorResponse
import com.skydoves.sandwich.ApiResponse
import timber.log.Timber

class ErrorResponseMapper<T> {
    fun map(apiErrorResponse: ApiResponse.Failure.Error<*>): ErrorResponse<T> {
        val response = Gson().fromJson(
            apiErrorResponse.errorBody?.charStream(),
            ErrorResponse::class.java
        )
        val code = apiErrorResponse.statusCode.code
        return ErrorResponse(code, response.message,null)
    }

    fun map(apiErrorResponse: ApiResponse.Failure.Exception<*>): ErrorResponse<T> {
        Timber.e(apiErrorResponse.message)
        return ErrorResponse(0, "Terjadi Masalah pada koneksi anda", null)
    }

    fun map(exception: Exception): ErrorResponse<T> {
        return ErrorResponse(0, exception.message,null)
    }
}