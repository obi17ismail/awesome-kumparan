package com.obi.awesomekumparan.repositories

import androidx.annotation.WorkerThread
import com.obi.awesomekumparan.data.response.users.AlbumPhotoResponse
import com.obi.awesomekumparan.data.response.users.AlbumResponse
import com.obi.awesomekumparan.data.response.users.UserResponse
import com.obi.awesomekumparan.data.uiresponse.BaseUiResponse
import com.obi.awesomekumparan.network.RemoteDataSource
import com.obi.awesomekumparan.utils.resultBaseResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : Repository {
    @WorkerThread
    fun getUser() = flow {
        val response = remoteDataSource.getUser()
        response.resultBaseResponse(this)
    }.onStart {
        val loading = BaseUiResponse.loading<List<UserResponse>>(null)
        emit(loading)
    }.flowOn(Dispatchers.IO)

    @WorkerThread
    fun getUserId(id: String) = flow {
        val response = remoteDataSource.getUserId(id)
        response.resultBaseResponse(this)
    }.onStart {
        val loading = BaseUiResponse.loading<UserResponse>(null)
        emit(loading)
    }.flowOn(Dispatchers.IO)

    // USER ALBUM
    @WorkerThread
    fun getAlbumUserId(userId: String) = flow {
        val response = remoteDataSource.getAlbumUserId(userId)
        response.resultBaseResponse(this)
    }.onStart {
        val loading = BaseUiResponse.loading<List<AlbumResponse>>(null)
        emit(loading)
    }.flowOn(Dispatchers.IO)

    @WorkerThread
    fun getAlbumIdPhoto(id: String) = flow {
        val response = remoteDataSource.getAlbumIdPhoto(id)
        response.resultBaseResponse(this)
    }.onStart {
        val loading = BaseUiResponse.loading<List<AlbumPhotoResponse>>(null)
        emit(loading)
    }.flowOn(Dispatchers.IO)
}