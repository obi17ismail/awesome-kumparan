package com.obi.awesomekumparan.repositories

import androidx.annotation.WorkerThread
import com.obi.awesomekumparan.data.response.posts.CommentResponse
import com.obi.awesomekumparan.data.response.posts.DataPostResponse
import com.obi.awesomekumparan.data.uiresponse.BaseUiResponse
import com.obi.awesomekumparan.network.RemoteDataSource
import com.obi.awesomekumparan.utils.resultBaseResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : Repository {
    @WorkerThread
    fun getPosts() = flow {
        val response = remoteDataSource.getPosts()
        response.resultBaseResponse(this)
    }.onStart {
        val loading = BaseUiResponse.loading<List<DataPostResponse>>(null)
        emit(loading)
    }.flowOn(Dispatchers.IO)

    fun getPostId(id: String) = flow {
        val response = remoteDataSource.getPostId(id)
        response.resultBaseResponse(this)
    }.onStart {
        val loading = BaseUiResponse.loading<DataPostResponse>(null)
        emit(loading)
    }.flowOn(Dispatchers.IO)

    fun getCommentPostId(postId: String) = flow {
        val response = remoteDataSource.getCommentPostId(postId)
        response.resultBaseResponse(this)
    }.onStart {
        val loading = BaseUiResponse.loading<List<CommentResponse>>(null)
        emit(loading)
    }.flowOn(Dispatchers.IO)
}