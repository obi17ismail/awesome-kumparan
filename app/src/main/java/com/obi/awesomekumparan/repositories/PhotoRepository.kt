package com.obi.awesomekumparan.repositories

import androidx.annotation.WorkerThread
import com.obi.awesomekumparan.data.response.users.AlbumPhotoResponse
import com.obi.awesomekumparan.data.uiresponse.BaseUiResponse
import com.obi.awesomekumparan.network.RemoteDataSource
import com.obi.awesomekumparan.utils.resultBaseResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

class PhotoRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : Repository {
    @WorkerThread
    fun getPhotos() = flow {
        val response = remoteDataSource.getPhotos()
        response.resultBaseResponse(this)
    }.onStart {
        val loading = BaseUiResponse.loading<List<AlbumPhotoResponse>>(null)
        emit(loading)
    }.flowOn(Dispatchers.IO)

    fun getPhotoId(id: String) = flow {
        val response = remoteDataSource.getPhotoId(id)
        response.resultBaseResponse(this)
    }.onStart {
        val loading = BaseUiResponse.loading<AlbumPhotoResponse>(null)
        emit(loading)
    }.flowOn(Dispatchers.IO)
}