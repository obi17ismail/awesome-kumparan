package com.obi.awesomekumparan.viewmodel.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.obi.awesomekumparan.data.response.users.AlbumPhotoResponse
import com.obi.awesomekumparan.data.response.users.AlbumResponse
import com.obi.awesomekumparan.data.response.users.UserResponse
import com.obi.awesomekumparan.data.uiresponse.BaseUiResponse
import com.obi.awesomekumparan.repositories.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(private val userRepository: UserRepository) :
    ViewModel() {
    fun getUser(): LiveData<BaseUiResponse<List<UserResponse>>> {
        return userRepository.getUser().asLiveData(viewModelScope.coroutineContext)
    }
    fun getUserId(id: String): LiveData<BaseUiResponse<UserResponse>> {
        return userRepository.getUserId(id).asLiveData(viewModelScope.coroutineContext)
    }

    // USER ALBUM
    fun getAlbumUserId(userId: String): LiveData<BaseUiResponse<List<AlbumResponse>>> {
        return userRepository.getAlbumUserId(userId).asLiveData(viewModelScope.coroutineContext)
    }
    fun getAlbumIdPhoto(id: String): LiveData<BaseUiResponse<List<AlbumPhotoResponse>>> {
        return userRepository.getAlbumIdPhoto(id).asLiveData(viewModelScope.coroutineContext)
    }
}