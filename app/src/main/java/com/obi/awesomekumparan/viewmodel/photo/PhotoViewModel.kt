package com.obi.awesomekumparan.viewmodel.photo

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.obi.awesomekumparan.data.response.users.AlbumPhotoResponse
import com.obi.awesomekumparan.data.uiresponse.BaseUiResponse
import com.obi.awesomekumparan.repositories.PhotoRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PhotoViewModel @Inject constructor(private val photoRepository: PhotoRepository) :
    ViewModel() {
    fun getPhotos(): LiveData<BaseUiResponse<List<AlbumPhotoResponse>>> {
        return photoRepository.getPhotos().asLiveData(viewModelScope.coroutineContext)
    }
    fun getPhotoId(id: String): LiveData<BaseUiResponse<AlbumPhotoResponse>> {
        return photoRepository.getPhotoId(id).asLiveData(viewModelScope.coroutineContext)
    }
}