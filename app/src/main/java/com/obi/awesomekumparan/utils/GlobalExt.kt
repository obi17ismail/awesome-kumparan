package com.obi.awesomekumparan.utils

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import com.obi.awesomekumparan.data.uiresponse.BaseUiResponse
import com.obi.awesomekumparan.data.uiresponse.ErrorResponseMapper
import com.obi.awesomekumparan.utils.AppConstant.DATA_IS_EMPTY
import com.obi.awesomekumparan.utils.AppConstant.SOMETHING_WRONG
import com.skydoves.sandwich.ApiResponse
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnException
import com.skydoves.sandwich.suspendOnSuccess
import kotlinx.coroutines.flow.FlowCollector

suspend fun <T> ApiResponse<T>.resultBaseResponse(flowCollector: FlowCollector<BaseUiResponse<T>>) {
    try {
        suspendOnSuccess {
            val code = statusCode.code
            val responseData = data
            val result: BaseUiResponse<T> = if (code == 201 || code == 200) {
                if (responseData != null) {
                    BaseUiResponse.success(responseData, code)
                } else {
                    BaseUiResponse.error(DATA_IS_EMPTY, code = code)
                }
            } else {
                BaseUiResponse.error(SOMETHING_WRONG, code = code)
            }

            flowCollector.emit(result)
        }.suspendOnError {
            val response = ErrorResponseMapper<T>().map(this)
            val result =
                BaseUiResponse.error(response.message, response.data, code = response.statusCode)
            flowCollector.emit(result)
        }.suspendOnException {
            this.exception.printStackTrace()
            val response = ErrorResponseMapper<T>().map(this)
            val result = BaseUiResponse.error(response.message, response.data)
            flowCollector.emit(result)
        }
    } catch (exception: Exception) {
        exception.printStackTrace()
        val response = ErrorResponseMapper<T>().map(exception)
        val result = BaseUiResponse.error(response.message, response.data)
        flowCollector.emit(result)
    }
}

fun NavController.navigateSafely(
    @IdRes resId: Int,
    args: Bundle? = null,
    navOptions: NavOptions? = null,
    navExtras: Navigator.Extras? = null
) {
    val action = currentDestination?.getAction(resId) ?: graph.getAction(resId)
    if(action != null && currentDestination?.id != action.destinationId) {
        navigate(resId, args, navOptions, navExtras)
    }
}
