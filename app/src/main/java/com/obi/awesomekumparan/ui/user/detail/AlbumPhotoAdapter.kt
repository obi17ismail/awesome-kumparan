package com.obi.awesomekumparan.ui.user.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.obi.awesomekumparan.R
import com.obi.awesomekumparan.data.response.users.AlbumPhotoResponse
import com.obi.awesomekumparan.databinding.ItemPhotoBinding
import com.obi.awesomekumparan.ui.user.photo.PhotoDetailFragment.Companion.ID_PHOTO
import com.obi.awesomekumparan.utils.AutoUpdateableAdapter
import com.obi.awesomekumparan.utils.image
import com.obi.awesomekumparan.utils.navigateSafely
import kotlin.properties.Delegates

class AlbumPhotoAdapter (listPost: MutableList<AlbumPhotoResponse>) :
    RecyclerView.Adapter<AlbumPhotoAdapter.ViewHolder>(), AutoUpdateableAdapter {
    private var items: List<AlbumPhotoResponse> by Delegates.observable(listPost) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemPhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListPhoto(datas: MutableList<AlbumPhotoResponse>) {
        items = datas
    }

    override fun getItemCount(): Int = if (items.size > 9) 9 else items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position])

    class ViewHolder(val binding: ItemPhotoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(photo: AlbumPhotoResponse) {
            binding.apply {
                ivPhoto.image(photo.thumbnailUrl)

                itemView.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putString(ID_PHOTO, photo.id)
                    it.findNavController().navigateSafely(
                        R.id.action_userDetailFragment_to_photoDetailFragment,
                        args = bundle
                    )
                }
            }
        }
    }

}