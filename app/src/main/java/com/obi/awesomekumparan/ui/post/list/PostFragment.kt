package com.obi.awesomekumparan.ui.post.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.obi.awesomekumparan.R
import com.obi.awesomekumparan.base.BindingFragment
import com.obi.awesomekumparan.data.response.posts.DataPostResponse
import com.obi.awesomekumparan.data.response.users.UserResponse
import com.obi.awesomekumparan.data.uiresponse.BaseUiResponse
import com.obi.awesomekumparan.data.uiresponse.Status
import com.obi.awesomekumparan.databinding.FragmentPostBinding
import com.obi.awesomekumparan.utils.showToast
import com.obi.awesomekumparan.utils.toGone
import com.obi.awesomekumparan.utils.toVisible
import com.obi.awesomekumparan.viewmodel.post.PostViewModel
import com.obi.awesomekumparan.viewmodel.user.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class PostFragment : BindingFragment<FragmentPostBinding>() {
    private val postViewModel: PostViewModel by viewModels()
    private val userViewModel: UserViewModel by viewModels()

    override val bindingInflater: (LayoutInflater) -> ViewBinding
        get() = FragmentPostBinding::inflate

    private lateinit var dataUsers: MutableList<UserResponse>

    // Load item per page
    private var isScrollLoading: Boolean = false
    private var currentPerPage = 10

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar()
        initUser()
        initPost()
        setupRecycler()
        addOnScroll()
    }

    private fun toolbar() {
        binding.apply {
            (activity as AppCompatActivity).setSupportActionBar(iAppBar.toolbar)
            (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
            iAppBar.tvToolbar.text = getString(R.string.list_post)
        }
    }

    // GET USER
    private fun initUser() {
        userViewModel.getUser().observe(this, {
            responseUser(it)
        })
    }

    private fun responseUser(response: BaseUiResponse<List<UserResponse>>) {
        binding.apply {
            when (response.status) {
                Status.ERROR -> {
                    val errorMessage = "${response.code} - ${response.message}"
                    Timber.e("Error: $errorMessage")
                    requireContext().showToast(errorMessage)
                }

                Status.SUCCESS -> {
                    val data = response.data!!
                    dataUsers = data.toMutableList()
                }
            }

        }
    }
    // END GET USER

    // GET POSTS
    private fun initPost() {
        postViewModel.getPosts().observe(this, {
            responsePost(it)
        })
    }

    private fun responsePost(response: BaseUiResponse<List<DataPostResponse>>) {
        binding.apply {
            when (response.status) {
                Status.LOADING -> {
                    if(isScrollLoading){
                        pbScroll.toVisible()
                    } else {
                        swipePost.isRefreshing = true
                    }
                }
                Status.ERROR -> {
                    pbScroll.toGone()
                    swipePost.isRefreshing = false
                    val errorMessage = "${response.code} - ${response.message}"
                    Timber.e("Error: $errorMessage")
                    requireContext().showToast(errorMessage)
                }

                Status.SUCCESS -> {
                    isScrollLoading = false
                    pbScroll.toGone()
                    swipePost.isRefreshing = false
                    val data = response.data!!
                    if (data.isNotEmpty()) {
                        var dataPosts: List<DataPostResponse> = emptyList()
                        rvPost.toVisible()

                        // Load item per page
                        data.take(currentPerPage).forEach {
                            dataPosts = dataPosts + listOf(it)
                        }
                        rvPost.adapter?.let { adapter ->
                            if (adapter is PostAdapter) {
                                val datas = dataPosts.toMutableList()
                                adapter.setListPost(datas, dataUsers)
                            }
                        }
                    } else {
                        tvEmpty.toVisible()
                    }

                }
            }

        }
    }
    // END GET POSTS

    private fun setupRecycler() {
        binding.apply {
            swipePost.setOnRefreshListener {
                initPost()
            }
            rvPost.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = PostAdapter(mutableListOf(), mutableListOf())
            }
        }
    }

    private fun addOnScroll() {
        // Attaches scrollListener with RecyclerView
        binding.apply {
            rvPost.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
                    if (!isScrollLoading) {
                        // findLastCompletelyVisibleItemPostition() returns position of last fully visible view.
                        // It checks, fully visible view is the last one.
                        if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == currentPerPage - 1) {
                            currentPerPage += 10
                            isScrollLoading = true
                            initPost()
                        }
                    }
                }
            })
        }
    }

}