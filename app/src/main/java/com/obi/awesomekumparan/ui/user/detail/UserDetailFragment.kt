package com.obi.awesomekumparan.ui.user.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewbinding.ViewBinding
import com.obi.awesomekumparan.R
import com.obi.awesomekumparan.base.BindingFragment
import com.obi.awesomekumparan.data.response.users.AlbumPhotoResponse
import com.obi.awesomekumparan.data.response.users.AlbumResponse
import com.obi.awesomekumparan.data.response.users.UserResponse
import com.obi.awesomekumparan.data.uiresponse.BaseUiResponse
import com.obi.awesomekumparan.data.uiresponse.Status
import com.obi.awesomekumparan.databinding.FragmentUserDetailBinding
import com.obi.awesomekumparan.utils.showToast
import com.obi.awesomekumparan.utils.toVisible
import com.obi.awesomekumparan.viewmodel.photo.PhotoViewModel
import com.obi.awesomekumparan.viewmodel.user.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class UserDetailFragment : BindingFragment<FragmentUserDetailBinding>() {
    private val userViewModel: UserViewModel by viewModels()
    private val photoViewModel: PhotoViewModel by viewModels()

    override val bindingInflater: (LayoutInflater) -> ViewBinding
        get() = FragmentUserDetailBinding::inflate

    private lateinit var dataPhotos: MutableList<AlbumPhotoResponse>

    var idUser: String = ""

    companion object {
        val ID_USER = "ID_USER"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar()
        initUserDetail()
        setupRecycler()
    }

    private fun toolbar() {
        binding.apply {
            (activity as AppCompatActivity).setSupportActionBar(iAppBar.toolbar)
            (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
            iAppBar.tvToolbar.text = getString(R.string.user_detail)
            iAppBar.toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_white_24)
            iAppBar.toolbar.setNavigationOnClickListener {
                findNavController().popBackStack()
            }
        }
    }

    // GET USER DETAIL
    private fun initUserDetail() {
        val idUser = arguments?.getString(ID_USER)
        userViewModel.getUserId(idUser!!).observe(this, {
            responseUserDetail(it)
        })
    }

    private fun responseUserDetail(response: BaseUiResponse<UserResponse>) {
        binding.apply {
            when (response.status) {
                Status.LOADING -> {
                    swipeUser.isRefreshing = true
                }
                Status.ERROR -> {
                    val errorMessage = "${response.code} - ${response.message}"
                    Timber.e("Error: $errorMessage")
                    requireContext().showToast(errorMessage)
                }

                Status.SUCCESS -> {
                    val data = response.data!!
                    initUserDetailView(data)
                }
            }

        }
    }

    @SuppressLint("SetTextI18n")
    private fun initUserDetailView(data: UserResponse) {
        initPhoto()
        idUser = data.id
        val dataAddress = data.address!!
        val dataCompany = data.company!!
        binding.apply {
            tvUserName.text = data.name
            tvEmail.text = data.email
            tvAddress.text = "${dataAddress.street}, ${dataAddress.suite}, ${dataAddress.city} ${dataAddress.zipcode}"

            tvCompanyName.text = "Name: ${dataCompany.name}"
            tvCompanyCatch.text = "Catch Phrase: ${dataCompany.catchPhrase}"
            tvCompanyBs.text = "BS: ${dataCompany.bs}"
        }
    }
    // END GET USER DETAIL

    // GET PHOTO
    private fun initPhoto() {
        photoViewModel.getPhotos().observe(this, {
            responsePhoto(it)
        })
    }

    private fun responsePhoto(response: BaseUiResponse<List<AlbumPhotoResponse>>) {
        binding.apply {
            when (response.status) {
                Status.ERROR -> {
                    val errorMessage = "${response.code} - ${response.message}"
                    Timber.e("Error: $errorMessage")
                    requireContext().showToast(errorMessage)
                }

                Status.SUCCESS -> {
                    val data = response.data!!
                    dataPhotos = data.toMutableList()
                    initAlbum(idUser)
                }
            }

        }
    }
    // END GET PHOTO

    // GET ALBUM
    private fun initAlbum(idUser: String) {
        userViewModel.getAlbumUserId(idUser).observe(this, {
            responseAlbum(it)
        })
    }

    private fun responseAlbum(response: BaseUiResponse<List<AlbumResponse>>) {
        binding.apply {
            when (response.status) {
                Status.ERROR -> {
                    swipeUser.isRefreshing = false
                    val errorMessage = "${response.code} - ${response.message}"
                    Timber.e("Error: $errorMessage")
                    requireContext().showToast(errorMessage)
                }

                Status.SUCCESS -> {
                    swipeUser.isRefreshing = false
                    val data = response.data!!
                    if (data.isNotEmpty()) {
                        rvAlbum.toVisible()
                        rvAlbum.adapter?.let { adapter ->
                            if (adapter is AlbumAdapter) {
                                val datas = data.toMutableList()
                                adapter.setListAlbum(datas, dataPhotos)
                            }
                        }
                    }
                }
            }

        }
    }
    // END GET ALBUM

    private fun setupRecycler() {
        binding.apply {
            swipeUser.setOnRefreshListener {
                initUserDetail()
            }
            rvAlbum.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = AlbumAdapter(mutableListOf(), mutableListOf())
            }
        }
    }
}