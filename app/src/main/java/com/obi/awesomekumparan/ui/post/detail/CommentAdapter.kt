package com.obi.awesomekumparan.ui.post.detail

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.obi.awesomekumparan.data.response.posts.CommentResponse
import com.obi.awesomekumparan.databinding.ItemCommentBinding
import com.obi.awesomekumparan.utils.AutoUpdateableAdapter
import kotlin.properties.Delegates

class CommentAdapter (listPost: MutableList<CommentResponse>) :
    RecyclerView.Adapter<CommentAdapter.ViewHolder>(), AutoUpdateableAdapter {
    private var items: List<CommentResponse> by Delegates.observable(listPost) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemCommentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListComment(datas: MutableList<CommentResponse>) {
        items = datas
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position])

    class ViewHolder(val binding: ItemCommentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(comment: CommentResponse) {
            binding.apply {
                tvUserName.text = comment.email
                tvBody.text = comment.body
            }
        }
    }

}