package com.obi.awesomekumparan.ui.post.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewbinding.ViewBinding
import com.obi.awesomekumparan.R
import com.obi.awesomekumparan.base.BindingFragment
import com.obi.awesomekumparan.data.response.posts.CommentResponse
import com.obi.awesomekumparan.data.response.posts.DataPostResponse
import com.obi.awesomekumparan.data.response.users.UserResponse
import com.obi.awesomekumparan.data.uiresponse.BaseUiResponse
import com.obi.awesomekumparan.data.uiresponse.Status
import com.obi.awesomekumparan.databinding.FragmentPostDetailBinding
import com.obi.awesomekumparan.ui.user.detail.UserDetailFragment.Companion.ID_USER
import com.obi.awesomekumparan.utils.navigateSafely
import com.obi.awesomekumparan.utils.showToast
import com.obi.awesomekumparan.utils.toVisible
import com.obi.awesomekumparan.viewmodel.post.PostViewModel
import com.obi.awesomekumparan.viewmodel.user.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class PostDetailFragment : BindingFragment<FragmentPostDetailBinding>() {
    private val postViewModel: PostViewModel by viewModels()
    private val userViewModel: UserViewModel by viewModels()

    override val bindingInflater: (LayoutInflater) -> ViewBinding
        get() = FragmentPostDetailBinding::inflate

    companion object {
        val ID_POST = "ID_POST"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar()
        initDetailPost()
        setupRecycler()
    }

    private fun toolbar() {
        binding.apply {
            (activity as AppCompatActivity).setSupportActionBar(iAppBar.toolbar)
            (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
            iAppBar.tvToolbar.text = getString(R.string.post_detail)
            iAppBar.toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_white_24)
            iAppBar.toolbar.setNavigationOnClickListener {
                findNavController().popBackStack()
            }
        }
    }

    // GET DETAIL POST
    private fun initDetailPost() {
        val idPost = arguments?.getString(ID_POST)
        postViewModel.getPostId(idPost!!).observe(this, {
            responseDetailPost(it)
        })
    }

    private fun responseDetailPost(response: BaseUiResponse<DataPostResponse>) {
        binding.apply {
            when (response.status) {
                Status.LOADING -> {
                    swipePost.isRefreshing = true
                }
                Status.ERROR -> {
                    val errorMessage = "${response.code} - ${response.message}"
                    Timber.e("Error: $errorMessage")
                    requireContext().showToast(errorMessage)
                }

                Status.SUCCESS -> {
                    val data = response.data!!
                    initDetailPostView(data)
                }
            }

        }
    }

    private fun initDetailPostView(data: DataPostResponse) {
        initUser(data.userId)
        initComment(data.id)
        binding.apply {
            tvTitle.text = data.title
            tvBody.text = data.body
        }
    }
    // END GET DETAIL POST

    // GET USER
    private fun initUser(idUser: String) {
        userViewModel.getUserId(idUser).observe(this, {
            responseUser(it)
        })
    }

    private fun responseUser(response: BaseUiResponse<UserResponse>) {
        binding.apply {
            when (response.status) {
                Status.ERROR -> {
                    val errorMessage = "${response.code} - ${response.message}"
                    Timber.e("Error: $errorMessage")
                    requireContext().showToast(errorMessage)
                }

                Status.SUCCESS -> {
                    val data = response.data!!
                    tvUserName.text = data.name
                    tvUserName.setOnClickListener {
                        val bundle = Bundle()
                        bundle.putString(ID_USER, data.id)
                        it.findNavController().navigateSafely(
                            R.id.action_postDetailFragment_to_userDetailFragment,
                            args = bundle
                        )
                    }
                }
            }

        }
    }
    // END GET USER

    // GET COMMENT
    private fun initComment(idPost: String) {
        postViewModel.getCommentPostId(idPost).observe(this, {
            responseComment(it)
        })
    }

    private fun responseComment(response: BaseUiResponse<List<CommentResponse>>) {
        binding.apply {
            when (response.status) {
                Status.ERROR -> {
                    swipePost.isRefreshing = false
                    val errorMessage = "${response.code} - ${response.message}"
                    Timber.e("Error: $errorMessage")
                    requireContext().showToast(errorMessage)
                }

                Status.SUCCESS -> {
                    swipePost.isRefreshing = false
                    val data = response.data!!
                    if (data.isNotEmpty()) {
                        rvComment.toVisible()
                        rvComment.adapter?.let { adapter ->
                            if (adapter is CommentAdapter) {
                                val datas = data.toMutableList()
                                adapter.setListComment(datas)
                            }
                        }
                    }
                }
            }

        }
    }
    // END GET COMMENT

    private fun setupRecycler() {
        binding.apply {
            swipePost.setOnRefreshListener {
                initDetailPost()
            }
            rvComment.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = CommentAdapter(mutableListOf())
            }
        }
    }
}