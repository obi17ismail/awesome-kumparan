package com.obi.awesomekumparan.ui.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.obi.awesomekumparan.R
import com.obi.awesomekumparan.base.BindingFragment
import com.obi.awesomekumparan.databinding.FragmentSplashBinding
import com.obi.awesomekumparan.utils.AppConstant.SPLASH_DURATION
import com.obi.awesomekumparan.utils.navigateSafely

class SplashFragment : BindingFragment<FragmentSplashBinding>() {
    override val bindingInflater: (LayoutInflater) -> ViewBinding
        get() = FragmentSplashBinding::inflate

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            findNavController().navigateSafely(
                R.id.action_splashFragment_to_postFragment,
                navOptions = NavOptions.Builder()
                    .setPopUpTo(R.id.splashFragment,
                        true).build()
            )
        },SPLASH_DURATION)
    }
}