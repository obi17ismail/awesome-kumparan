package com.obi.awesomekumparan.ui.user.detail

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.obi.awesomekumparan.data.response.users.AlbumPhotoResponse
import com.obi.awesomekumparan.data.response.users.AlbumResponse
import com.obi.awesomekumparan.databinding.ItemAlbumBinding
import com.obi.awesomekumparan.utils.AutoUpdateableAdapter
import kotlin.properties.Delegates

class AlbumAdapter (
    listPost: MutableList<AlbumResponse>,
    private var listPhoto: MutableList<AlbumPhotoResponse>) :
    RecyclerView.Adapter<AlbumAdapter.ViewHolder>(), AutoUpdateableAdapter {

    private var items: List<AlbumResponse> by Delegates.observable(listPost) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemAlbumBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListAlbum(datas: MutableList<AlbumResponse>, photos: MutableList<AlbumPhotoResponse>) {
        items = datas
        listPhoto = photos
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position], listPhoto)

    class ViewHolder(val binding: ItemAlbumBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(album: AlbumResponse, photos: List<AlbumPhotoResponse>) {
            var dataPhotos: List<AlbumPhotoResponse> = emptyList()
            binding.apply {
                tvAlbum.text = album.title
                rvPhoto.apply {
                    layoutManager = GridLayoutManager(context, 3)
                    adapter = AlbumPhotoAdapter(mutableListOf())
                }

                // Get photo by album id
                photos.forEach {
                    if(it.albumId == album.id){
                        dataPhotos = dataPhotos + listOf(it)
                    }
                }
                rvPhoto.adapter?.let { adapter ->
                    if (adapter is AlbumPhotoAdapter) {
                        val datas = dataPhotos.toMutableList()
                        adapter.setListPhoto(datas)
                    }
                }
            }
        }
    }

}