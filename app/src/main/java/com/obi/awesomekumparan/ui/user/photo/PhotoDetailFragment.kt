package com.obi.awesomekumparan.ui.user.photo

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.obi.awesomekumparan.R
import com.obi.awesomekumparan.base.BindingFragment
import com.obi.awesomekumparan.data.response.users.AlbumPhotoResponse
import com.obi.awesomekumparan.data.uiresponse.BaseUiResponse
import com.obi.awesomekumparan.data.uiresponse.Status
import com.obi.awesomekumparan.databinding.FragmentPhotoDetailBinding
import com.obi.awesomekumparan.utils.image
import com.obi.awesomekumparan.utils.showToast
import com.obi.awesomekumparan.viewmodel.photo.PhotoViewModel
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class PhotoDetailFragment : BindingFragment<FragmentPhotoDetailBinding>() {
    private val photoViewModel: PhotoViewModel by viewModels()

    override val bindingInflater: (LayoutInflater) -> ViewBinding
        get() = FragmentPhotoDetailBinding::inflate

    companion object {
        val ID_PHOTO = "ID_PHOTO"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar()
        initPhotoDetail()
    }

    private fun toolbar() {
        binding.apply {
            (activity as AppCompatActivity).setSupportActionBar(iAppBar.toolbar)
            (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
            iAppBar.toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_white_24)
            iAppBar.toolbar.setNavigationOnClickListener {
                findNavController().popBackStack()
            }
        }
    }

    // GET PHOTO DETAIL
    private fun initPhotoDetail() {
        val idPhoto = arguments?.getString(ID_PHOTO)
        photoViewModel.getPhotoId(idPhoto!!).observe(this, {
            responsePhotoDetail(it)
        })
    }

    private fun responsePhotoDetail(response: BaseUiResponse<AlbumPhotoResponse>) {
        binding.apply {
            when (response.status) {
                Status.ERROR -> {
                    val errorMessage = "${response.code} - ${response.message}"
                    Timber.e("Error: $errorMessage")
                    requireContext().showToast(errorMessage)
                }

                Status.SUCCESS -> {
                    val data = response.data!!
                    initUserDetailView(data)
                }
            }

        }
    }

    @SuppressLint("SetTextI18n")
    private fun initUserDetailView(data: AlbumPhotoResponse) {
        binding.apply {
            iAppBar.tvToolbar.text = data.title
            ivPhoto.image(data.url)
        }
    }
    // END GET PHOTO DETAIL
}