package com.obi.awesomekumparan.ui.post.list

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.obi.awesomekumparan.R
import com.obi.awesomekumparan.data.response.posts.DataPostResponse
import com.obi.awesomekumparan.databinding.ItemPostBinding
import com.obi.awesomekumparan.utils.AutoUpdateableAdapter
import com.obi.awesomekumparan.utils.navigateSafely
import kotlin.properties.Delegates
import android.os.Bundle
import com.obi.awesomekumparan.data.response.users.UserResponse
import com.obi.awesomekumparan.ui.post.detail.PostDetailFragment.Companion.ID_POST

class PostAdapter (
    listPost: MutableList<DataPostResponse>,
    private var listUser: MutableList<UserResponse>) :
    RecyclerView.Adapter<PostAdapter.ViewHolder>(), AutoUpdateableAdapter {
    private var items: List<DataPostResponse> by Delegates.observable(listPost) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListPost(datas: MutableList<DataPostResponse>, users: MutableList<UserResponse>) {
        items = datas
        listUser = users
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position], listUser)

    class ViewHolder(val binding: ItemPostBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(post: DataPostResponse, user: List<UserResponse>) {
            binding.apply {
                tvTitle.text = post.title
                tvBody.text = post.body

                // Get user info
                user.forEach {
                    if(it.id == post.userId){
                        tvUserName.text = it.name
                        tvUserCompany.text = it.company!!.name
                    }
                }

                itemView.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putString(ID_POST, post.id)
                    it.findNavController().navigateSafely(
                        R.id.action_postFragment_to_postDetailFragment,
                        args = bundle
                    )
                }
            }
        }
    }

}